/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package contract_process

import (
	"chainmaker-oracle/config"
	"chainmaker-oracle/contract_process/vrf_utils"
	"chainmaker-oracle/models"
	"context"
	"errors"
	"fmt"
	"reflect"
	"strconv"

	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	sdk "chainmaker.org/chainmaker/sdk-go/v2"
)

// ListInstalledOracleContracts 查询指定链上安装的预言机合约（通过http接口安装的合约）
// @param string
// @return []models.ContractInfo
// @return error
func (cp *ContractProcessor) ListInstalledOracleContracts(chainAlias string) ([]models.ContractInfo, error) {
	// 查询数据库中安装合约
	contracts, contractsErr := models.LoadContracts()
	if contractsErr != nil {
		cp.Log.Errorf("ListInstalledOracleContracts got error, error is : %s  ", contractsErr.Error())
		return nil, contractsErr
	}
	var retContract []models.ContractInfo
	// 过滤指定的链安装的合约
	for i := 0; i < len(contracts); i++ {
		if contracts[i].ChainAlias == chainAlias {
			retContract = append(retContract, contracts[i])
		}
	}
	return retContract, nil
}

// GetChainIdByChainAlias 根据chain alias查找对应的chain id
// @param string
// @return string
func GetChainIdByChainAlias(chainAlias string) string {
	for _, sdkConfig := range config.GlobalCFG.SDKS {
		if sdkConfig.ChainAlias == chainAlias {
			return sdkConfig.ChainId
		}
	}
	return ""
}

// DeployContractAndSubScribe 安装预言机合约，监听合约的query和finish事件
// @param string
// @param string
// @param string
// @param string
// @param common.RuntimeType
// @return *models.ContractInfo
// @return error
func (cp *ContractProcessor) DeployContractAndSubScribe(chainAlias, contractName,
	version, byteCodeStringOrFilePath string,
	runtime common.RuntimeType) (*models.ContractInfo, error) {
	versionNum, _ := strconv.Atoi(version)
	chainId := GetChainIdByChainAlias(chainAlias)
	// 构造合约数据库模型
	entry := ContractFileEntry{
		ContractName: contractName,
		ChainId:      chainId,
		ChainAlias:   chainAlias,
		Version:      versionNum,
		FilePath:     byteCodeStringOrFilePath,
		NeedOperate:  1,
	}
	// 安装合约，更新数据库
	deploySuccess, deployErr := cp.DeployAndUpdateDB(contractName,
		byteCodeStringOrFilePath, chainAlias, chainId, versionNum, 1)
	if deployErr != nil {
		cp.Log.Errorf("DeployContractAndSubScribe DeployContract err(%s),"+
			" chainAlias(%s) , contractName(%s), version(%s), "+
			"byteCodeStringOrFilePath(%s), runtimeType(%d)  ",
			deployErr.Error(), chainAlias, contractName, version,
			byteCodeStringOrFilePath, runtime)
		return nil, deployErr
	}
	if !deploySuccess {
		return nil, errors.New("internal server error")
	}
	// 查询链上安装的合约信息
	contractInfo, queryErr := models.QueryContractInfo(chainAlias, contractName)
	if queryErr != nil {
		cp.Log.Errorf("DeployContractAndSubScribe QueryContractInfo err(%s), entry(%+v)  ", queryErr.Error(), entry)
		return nil, queryErr
	}
	// 订阅合约的query事件
	go func() {
		_ = cp.SubscribeContracts(chainAlias, contractName, topicOracleQuery, int64(contractInfo.TopicQueryStart), -1)
	}()
	// 订阅合约的finish事件
	go func() {
		_ = cp.SubscribeContracts(chainAlias, contractName, topicOracleFinished, int64(contractInfo.TopicFinishStart), -1)
	}()
	return contractInfo, nil
}

// DeployContract 部署预言机合约
// @param string
// @param string
// @param string
// @param string
// @param common.RuntimeType
// @return error
func (cp *ContractProcessor) DeployContract(chainAlias, contractName, version, byteCodeStringOrFilePath string,
	runtime common.RuntimeType) error {
	// 部署合约
	chainClient, chainClientErr := cp.GetChainClient(chainAlias)
	if chainClientErr != nil {
		return chainClientErr
	}
	if chainClient == nil {
		return errors.New("chainAlias param error")
	}
	// 构造部署合约请求
	payload, payloadErr := chainClient.CreateContractCreatePayload(contractName,
		version, byteCodeStringOrFilePath, runtime,
		makeContractDeployOrUpgradeKV(chainAlias, version,
			contractName, vrf_utils.GlobalECVRF.PublicKeyBytes))
	if payloadErr != nil {
		cp.Log.Errorf("DeployContract err, %s  ", payloadErr.Error())
		return payloadErr
	}
	// 寻找一下链的sdk配置
	var chainCfg *config.SDKConfig
	for ind := 0; ind < len(config.GlobalCFG.SDKS); ind++ {
		if config.GlobalCFG.SDKS[ind].ChainAlias == chainAlias {
			chainCfg = &config.GlobalCFG.SDKS[ind]
		}
	}
	// an not find sdk config , whose chain alias match
	if chainCfg == nil {
		return errors.New(" no sdk config named " + chainAlias)
	}
	// 签名
	endorses, err := GetEndorsersWithAuthType(crypto.HashAlgoMap[chainClient.GetHashType()],
		chainClient.GetAuthType(), payload, chainCfg.Signs)

	if err != nil {
		cp.Log.Errorf("GetEndorsersWithAuthType err, err is %s  ", err.Error())
		return err
	}
	// 发送grpc请求
	resp, respErr := chainClient.SendContractManageRequest(payload, endorses, 10, true) // 参数
	if respErr != nil {
		cp.Log.Errorf("SendContractManageRequest err, %s  ", respErr.Error())
		return respErr
	}
	// 检查是否出错
	checkErr := CheckProposalRequestResp(resp, true)
	if checkErr != nil {
		cp.Log.Errorf("CheckProposalRequestResp err , %s  ", checkErr.Error())
		return checkErr
	}
	return nil
}

// UpdateContract 升级预言机合约
// @param string
// @param string
// @param string
// @param string
// @param common.RuntimeType
// @return error
func (cp *ContractProcessor) UpdateContract(chainAlias, contractName,
	version, byteCodeStringOrFilePath string,
	runtime common.RuntimeType) error {
	// 根据链alias找对应的grpc客户端
	chainClient, chainClientErr := cp.GetChainClient(chainAlias)
	if chainClientErr != nil {
		return chainClientErr
	}
	if chainClient == nil {
		return errors.New("chainAlias param error")
	}
	// 构造合约请求
	payload, payloadErr := chainClient.CreateContractUpgradePayload(contractName,
		version, byteCodeStringOrFilePath, runtime,
		makeContractDeployOrUpgradeKV(chainAlias, version,
			contractName, vrf_utils.GlobalECVRF.PublicKeyBytes))
	if payloadErr != nil {
		cp.Log.Errorf("UpdateContract err, %s  ", payloadErr.Error())
		return payloadErr
	}
	// 寻找一下链的sdk配置
	var chainCfg *config.SDKConfig
	for ind := 0; ind < len(config.GlobalCFG.SDKS); ind++ {
		if config.GlobalCFG.SDKS[ind].ChainAlias == chainAlias {
			chainCfg = &config.GlobalCFG.SDKS[ind]
		}
	}
	// an not find sdk config , whose chain alias match
	if chainCfg == nil {
		return errors.New(" no sdk config named " + chainAlias)
	}
	// 签名
	endorses, err := GetEndorsersWithAuthType(crypto.HashAlgoMap[chainClient.GetHashType()],
		chainClient.GetAuthType(), payload, chainCfg.Signs)

	if err != nil {
		cp.Log.Errorf("GetEndorsersWithAuthType err, err is %s  ", err.Error())
		return err
	}
	// 发送grpc
	resp, respErr := chainClient.SendContractManageRequest(payload, endorses, 10, true)
	if respErr != nil {
		cp.Log.Errorf("SendContractManageRequest err, err is %s  ", respErr.Error())
		return respErr
	}
	cp.Log.Info("UpdateContract success ", resp.Code, resp.Message, resp.String())
	// 检查结果
	checkErr := CheckProposalRequestResp(resp, true)
	if checkErr != nil {
		cp.Log.Errorf("CheckProposalRequestResp err , %s  ", checkErr.Error())
		return checkErr
	}
	return nil
}

// QueryContract 查询指定链上的合约方法
// @param string
// @param string
// @param string
// @param []*common.KeyValuePair
// @return *common.TxResponse
// @return error
func (cp *ContractProcessor) QueryContract(chainAlias,
	contractName, contractMethod string,
	kvs []*common.KeyValuePair) (*common.TxResponse, error) {
	// 首先根据chainAlias来查找chainId ，如果查不到报错
	chainClient, chainClientErr := cp.GetChainClient(chainAlias)
	if chainClientErr != nil {
		return nil, chainClientErr
	}
	if chainClient == nil {
		return nil, errors.New("chainAlias param error")
	}
	// 调用指定合约的指定方法（同步返回结果）
	resp, respErr := chainClient.QueryContract(contractName, contractMethod, kvs, -1)
	if respErr != nil {
		cp.Log.Errorf("QueryContract contractName(%s)"+
			" contractMethod(%s) got error (%s)  ",
			contractName, contractMethod, respErr.Error())
		return nil, respErr
	}
	return resp, nil
}

// InvokeContract 执行指定链上的合约方法
// @param string
// @param string
// @param string
// @param []*common.KeyValuePair
// @return error
func (cp *ContractProcessor) InvokeContract(chainAlias,
	contractName, contractMethod string,
	kvs []*common.KeyValuePair) error {
	txId := ""
	// 根据链alias找对应的grpc连接客户端
	chainClient, chainClientErr := cp.GetChainClient(chainAlias)
	if chainClientErr != nil {
		return chainClientErr
	}
	if chainClient == nil {
		return errors.New("chainAlias param error")
	}
	// 执行合约的指定方法
	resp, err := chainClient.InvokeContract(contractName,
		contractMethod, txId, kvs, -1, true)
	if err != nil {
		cp.Log.Errorf("InvokeContract err, %s  ", err.Error())
		return err
	}
	if resp == nil {
		cp.Log.Errorf("InvokeContract got no data")
		return errors.New("InvokeContract got no data")
	}
	cp.Log.Infof("InvokeContract resp , %s  ", resp.String())
	return nil
}

// SubscribeContracts 监听指定合约的指定topic
// @param string
// @param string
// @param string
// @param int64
// @param int64
// @return error
func (cp *ContractProcessor) SubscribeContracts(chainAlias,
	contractName, topic string, start, end int64) error {
	storeKey := fmt.Sprintf("%s#%s#%s", chainAlias, contractName, topic)
	// 根据链alias找对应的grpc连接客户端
	chainClient, chainClientErr := cp.GetChainClient(chainAlias)
	if chainClientErr != nil {
		cp.TopicInSubMp.Delete(storeKey)
		return chainClientErr
	}
	if chainClient == nil {
		cp.TopicInSubMp.Delete(storeKey)
		return errors.New("chainAlias param error")
	}
	// 链别名+合约名称+合约事件名称一起hash做key
	_, inProcess := cp.TopicInSubMp.Load(storeKey)
	if inProcess {
		cp.Log.Debugf("SubscribeContracts inprocess "+
			" ,start is %d , end is %d , chainAlias is %s, contractname is %s , topic is %s  ",
			start, end, chainAlias, contractName, topic)
		return nil
	}
	ctx, ctxCancel := context.WithCancel(context.Background())
	// 订阅链上下发事件
	messageChan, messageChanErr := chainClient.SubscribeContractEvent(ctx,
		start, end, contractName, topic)
	if messageChanErr != nil {
		cp.Log.Errorf("SubscribeContracts met error ,"+
			"start is %d , end is %d , chainAlias is %s, contractname is %s , topic is %s , err is %s  ",
			start, end, chainAlias, contractName, topic, messageChanErr.Error())
		ctxCancel()
		return messageChanErr
	}
	cp.Log.Infof("SubscribeContracts success ,"+
		"start is %d , end is %d , chainAlias is %s, contractname is %s , topic is %s  ",
		start, end, chainAlias, contractName, topic)
	// 同步信号增加一下
	cp.CloseSync.Add(1)
	cp.TopicInSubMp.Store(storeKey, struct{}{}) // 标记一下正在处理中
	defer func() {
		cp.CloseSync.Done()              // 同步信号退出时候清除一下
		cp.TopicInSubMp.Delete(storeKey) // 删除一下正在处理的任务
	}()
	for {
		select {
		case event, eventOk := <-messageChan:
			cp.Log.Info("get event")
			if eventOk {
				cp.Log.Infof("got event debug type(%+v) , origin (%+v)  ",
					reflect.TypeOf(event), event)
				eventStr, eventStrOk := event.(*common.ContractEventInfo)
				if eventStrOk {
					// 将消息放入消息通道
					cp.MessageChan <- eventStr
				}
			} else {
				// 与链的rpc出了问题
				cp.Log.Errorf("SubscribeContracts got error, "+
					"chainAlias is %s, contractname is %s , topic is %s ", chainAlias, contractName, topic)
				ctxCancel()
				return errors.New("SubscribeContracts got error")
			}
		case <-ctx.Done():
			cp.Log.Infof("SubscribeContracts end , start is %d ,"+
				" end is %d , chainAlias is %s, contractname is %s , topic is %s  ",
				start, end, chainAlias, contractName, topic)
			ctxCancel() // double cancel just for code detect
			return nil
		case <-cp.CloseChan:
			cp.Log.Infof("SubscribeContracts get signal quit ,"+
				" chainAlias is %s, contractname is %s , topic is %s  ",
				chainAlias, contractName, topic)
			ctxCancel()
		}
	}
}

// DeployAndUpdateDB 部署合约，将信息更新到数据库表
// @param string
// @param string
// @param string
// @param string
// @param int
// @param int
// @return bool
// @return error
func (cp *ContractProcessor) DeployAndUpdateDB(name,
	filePath, chainAlias, chainId string, version int, needOperate int) (bool, error) {
	// 部署合约
	deployErr := cp.DeployContract(chainAlias,
		name, strconv.Itoa(version), filePath, common.RuntimeType_DOCKER_GO)
	if deployErr != nil {
		cp.Log.Errorf("DeployAndUpdateDB chain error (%s)  ", deployErr.Error())
		return false, deployErr
	}
	// 更新到数据库
	insertErr := models.InsertContract(
		makeDbContractFromContractEntry(ContractFileEntry{
			ContractName: name,
			Version:      version,
			FilePath:     filePath,
			ChainId:      chainId,
			NeedOperate:  1,
			ChainAlias:   chainAlias,
		}))
	if insertErr != nil {
		cp.Log.Errorf("DeployAndUpdateDB db error (%s)  ", insertErr.Error())
		return false, insertErr
	}
	return true, nil
}

// UpdateContractAndUpdateDB 升级用户合约，将状态更新到数据库
// @param string
// @param string
// @param string
// @param int
// @param int
// @return bool
// @return error
func (cp *ContractProcessor) UpdateContractAndUpdateDB(name,
	filePath, chainAlias string, version int, needOperate int) (bool, error) {
	// 升级合约
	upgradeChainErr := cp.UpdateContract(chainAlias,
		name, strconv.Itoa(version), filePath, common.RuntimeType_DOCKER_GO)
	if upgradeChainErr != nil {
		cp.Log.Errorf("UpdateContractAndUpdateDB chain error (%s)  ", upgradeChainErr.Error())
		return false, upgradeChainErr
	}
	// 更新数据库
	upgradeErr := models.UpGradeContractInfo(chainAlias, name, version)
	if upgradeErr != nil {
		cp.Log.Errorf("UpdateContractAndUpdateDB db error, %s ", upgradeErr.Error())
		return false, upgradeErr
	}
	return true, nil
}

// SubscribeContractEvents 扫描安装的预言机合约，监听所有这些合约的query/finish事件
// @return error
func (cp *ContractProcessor) SubscribeContractEvents() error {
	dbChainContracts, dbChainContractsErr := ScanDbContracts() // 扫描数据库合约
	if dbChainContractsErr != nil {
		cp.Log.Errorf("SubscribeContractEvents load contract err, %s ", dbChainContractsErr)
		return dbChainContractsErr
	}
	// 订阅链上的安装合约的query和finish事件
	for chainAlias, contractMp := range dbChainContracts {
		for contractName, contractInfo := range contractMp {
			// 订阅链上的合约的query事件
			go func(alias, name string, info models.ContractInfo) {
				_ = cp.SubscribeContracts(alias, name, topicOracleQuery, int64(info.TopicQueryStart), -1)
			}(chainAlias, contractName, contractInfo)
			// 订阅链上的合约finish事件
			go func(alias, name string, info models.ContractInfo) {
				_ = cp.SubscribeContracts(alias, name, topicOracleFinished, int64(info.TopicFinishStart), -1)
			}(chainAlias, contractName, contractInfo)
		}
	}
	return nil
}

// ConsumeEvents 从链上消费事件，直到接收到服务器下发的关机信号
func (cp *ContractProcessor) ConsumeEvents() {
	for event := range cp.MessageChan {
		// 从消息通道中取一下数据，放到线程池子中进行执行
		subErr := cp.EventWorkerPool.Submit(func() {
			cp.dealEvent(event)
		})
		if subErr != nil {
			cp.Log.Errorf("ConsumeEvents submit pool error %s ", subErr.Error())
		}
	}
	// 消息通道关闭了
	cp.Log.Info("ConsumeEvents end")
	cp.CronManager.Stop()         // 关闭定时任务
	cp.ContractCronManager.Stop() // 关闭周期性监视任务
	cp.EventWorkerPool.Release()  // 关闭线程池子
	cp.Log.Info("ConsumeEvents resource shutdown")
	cp.SafeClose <- struct{}{}
}

// WatchSubscribeClose 监听事件关闭
func (cp *ContractProcessor) WatchSubscribeClose() {
	<-cp.CloseChan
	cp.Log.Info("WatchSubscribeClose get CloseChan")
	cp.CloseSync.Wait()
	cp.Log.Info("WatchSubscribeClose wait end")
	close(cp.MessageChan)
	cp.Log.Info("WatchSubscribeClose done")
}

// Stop 服务优雅退出
func (cp *ContractProcessor) Stop() {
	close(cp.CloseChan)
	<-cp.SafeClose // 等待剩余数据处理完毕
	cp.Log.Info("ContractProcessor stop end")
}

// GetChainClient 根据chain alias来获取与链上连接的grpc客户端（单飞）
// @param string
// @return *sdk.ChainClient
// @return error
func (cp *ContractProcessor) GetChainClient(chainAlias string) (*sdk.ChainClient, error) {
	// 单飞
	retO, retOErr, _ := cp.SingleBarrier.Do(chainAlias, func() (interface{}, error) {
		// 获取指定链的配置
		chainCFG, chainCFGOk := getSdkConfigByChainAlias(chainAlias)
		if !chainCFGOk {
			cp.Log.Warnf("GetChainClient find no chain(%s) ", chainAlias)
			return nil, nil
		}
		var chainClient *sdk.ChainClient
		// 查一下指定的链有没有正常运行的grpc客户端
		chainO, chainOk := cp.ChainClientMp.Load(chainAlias)
		if !chainOk {
			cp.Log.Errorf("GetChainClient no chain(%s) , chaincfgyml(%+v)  ", chainAlias, chainCFG)
			// 根据链的sdk配置构造一个grpc客户端对象
			sdkCli, sdkCliErr := CreateChainClientWithSDKConf(chainCFG.ConfigPath)
			if sdkCliErr != nil {
				cp.Log.Errorf("GetChainClient create chain error(%s),chainAlias(%s)  ", sdkCliErr.Error(), chainAlias)
				return nil, sdkCliErr
			}
			chainClient = sdkCli

		} else {
			chainClient, _ = chainO.(*sdk.ChainClient)
		}
		// ping一下，看看是否是通的
		rpcChainCFG, rpcErr := chainClient.GetChainConfig()
		if rpcErr != nil {
			cp.Log.Errorf("GetChainClient  get chain cfg error(%s) ", rpcErr.Error())
			_ = chainClient.Stop()              // 需要停掉
			cp.ChainClientMp.Delete(chainAlias) // ping不通，删除调
			return nil, rpcErr
		}
		cp.Log.Debugf("GetChainClient chain(%s) , cfgyml(%+v) , cfginfo(%+v) ", chainAlias, chainCFG, *rpcChainCFG)

		if !chainOk {
			cp.ChainClientMp.Store(chainAlias, chainClient)
		}
		return chainClient, nil
	})
	if retOErr != nil {
		return nil, retOErr
	}
	if retO == nil {
		return nil, fmt.Errorf("GetChainClient find no chain(%s)", chainAlias)
	}
	retClient, _ := retO.(*sdk.ChainClient)
	return retClient, nil

}
