/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package vrf_utils

import (
	"fmt"
	"testing"
)

func TestVerify(t *testing.T) {
	vdatas, vdataErr := GenerateKeys()
	if vdataErr != nil {
		fmt.Printf("generatekey err (%s)  ", vdataErr.Error())
		return
	}
	InitVRFProcessor(vdatas)
	fetcher := VRFGetter{}
	jbs, _ := fetcher.Fetch(("chain123xxxxwdwfgwegw12324tgegrwjrh"))
	fmt.Println(string(jbs))

	pi, _, err := Prove([]byte("chain123"))
	if err != nil {
		fmt.Println("prove error , ", err.Error())
		return
	}
	vb, ve := Verify(pi, []byte("chain123d"))
	if ve != nil {
		fmt.Println("verify error , ", ve.Error())
		return
	}
	fmt.Printf("pi(%+v) , check consist : %+v  ", pi, vb)
}
