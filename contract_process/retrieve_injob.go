/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package contract_process

import (
	"chainmaker-oracle/config"
	"chainmaker-oracle/models"
	"time"
)

// RetrieveInProcessingJob 从数据库中扫描出未完成的事件。拿到事件后，取争抢锁，如果抢到则处理。否则加个监控
// @param time.Time
// @return error
func (cp *ContractProcessor) RetrieveInProcessingJob(serverStartTime time.Time) error {

	queryPage := 0
	needBreak := false
	var inProcessStates []*models.EventState //未处理完的事件合集
	var fibonacciStates []*models.EventState //调用链失败的事件合集
	for !needBreak {
		// 分页查询未处理完的事件
		tempStates, tempStatesErr := models.QueryInProcessEvents(serverStartTime, queryPage)
		if tempStatesErr != nil {
			cp.Log.Errorf("RetrieveInProcessingJob query page(%d), err , %s", queryPage, tempStatesErr.Error())
			return tempStatesErr
		}
		for i := 0; i < len(tempStates); i++ {
			if tempStates[i].State == models.StateCallbackFailed {
				fibonacciStates = append(fibonacciStates, &tempStates[i])
			} else {
				inProcessStates = append(inProcessStates, &tempStates[i])
			}
		}
		queryPage = queryPage + 1
		if len(tempStates) < models.QueryStatePageSize {
			needBreak = true
		}
	}
	// 起一个携程处理fibonacci失败任务
	go func(fStates []*models.EventState) {
		for _, tState := range fStates {
			// 放入状态机
			_ = cp.Run(tState.ChainAlias, tState.TxId, tState.RequestId)
		}
	}(fibonacciStates)

	go func(states []*models.EventState) {

		for _, iState := range states {
			nowTime := time.Now()
			i30Before := nowTime.Add(-1 * time.Second * time.Duration(config.GlobalCFG.WorkCFG.ProcessOvertime))
			// 首先抢一把锁，查一下在超时配置时间内尚未进行过更新的事件，更新成功认为是抢到了锁
			updateLockErr := models.UpdateEventLock(&models.EventLock{
				RequestId: iState.RequestId,
				UpdatedAt: i30Before,
			})
			if updateLockErr == nil {
				// 抢到锁则放到状态机中运行
				_ = cp.Run(iState.ChainAlias, iState.TxId, iState.RequestId)
			} else {
				// 未拿到锁，加一个定时任务，后面再次检查改任务
				cp.Log.Warnf("RetrieveInProcessingJob updateEventLock error %s", updateLockErr.Error())
				cp.addWatchUnFinishedJob(iState.ChainAlias, iState.RequestId, iState.TxId)
			}
		}
	}(inProcessStates)
	return nil
}
