/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package http_server_process

import (
	"chainmaker-oracle/config"
	"chainmaker-oracle/contract_process"
	"chainmaker-oracle/logger"
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// HttpSrv 预言机http接口模块实例类
type HttpSrv struct {
	Router            *gin.Engine //路由
	Log               *zap.SugaredLogger
	BusinessProcessor *contract_process.ContractProcessor //业务注入
}

// NewHttpSrv 初始化一个http的server实例
func NewHttpSrv(businessP *contract_process.ContractProcessor) *HttpSrv {
	return &HttpSrv{
		Router:            gin.New(),
		Log:               logger.NewLogger(logger.ModuleHttpClient, &config.GlobalCFG.LogCFG),
		BusinessProcessor: businessP,
	}
}

// Listen http服务监听方法，死循环直到受到退出信号
func (srv *HttpSrv) Listen() {
	gin.SetMode(config.GlobalCFG.HttpServerCFG.RunMode)
	srv.Router.Use(gin.Recovery())
	srv.registerRouters() // 安装路由
	newHttpServer := &http.Server{
		Addr:    config.GlobalCFG.HttpServerCFG.Port,
		Handler: srv.Router,
	}
	// 注册信号
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		// 监听http端口
		srv.Log.Infof("http service start , port %s ", newHttpServer.Addr)
		if err := newHttpServer.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			srv.Log.Errorf("http server start err , %s ", err.Error())
		}
	}()
	<-stop
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer func() {
		cancel()
	}()
	// 优雅退出
	if err := newHttpServer.Shutdown(ctx); err != nil {
		srv.Log.Errorf("http service failed shutdown ,err %s ", err.Error())
		return
	}
	srv.Log.Info("http service success shutdown")
}

func (srv *HttpSrv) registerRouters() {
	srv.Router.POST("/v1/install_contract", srv.postInstallContract) // 安装合约
	srv.Router.POST("/v1/upgrade_contract", srv.postUpgradeContract) // 升级合约
	srv.Router.GET("/v1/list_contracts", srv.getContractList)        // 查询合约
	srv.Router.GET("/v1/health", srv.healthCheck)                    // 健康检查
	// vrf
	srv.Router.GET("/v1/vrf/get_public_key", srv.getPublicKey) // 公钥查询
	srv.Router.POST("/v1/vrf/verify", srv.postVerify)          // vrf验证
	srv.Router.POST("/v1/vrf/get_random", srv.postRandom)      // 获取vrf随机数
	// query
	srv.Router.POST("/v1/query/query_mysql", srv.getMysqlQuery) // 查询mysql数据
	srv.Router.POST("/v1/query/query_http", srv.getHttpQuery)   // 查询http接口数据
	srv.Router.POST("/v1/query/query_cross", srv.getCrossQuery) // 跨链查询

}
