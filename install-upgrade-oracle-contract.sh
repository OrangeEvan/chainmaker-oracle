#!/bin/bash 

#
# Copyright (C) BABEC. All rights reserved.
#
# SPDX-License-Identifier: Apache-2.0
#

contract_name=$(grep "oracle_contract_name:" ./config_files/smart_oracle.yml | awk '{print $2}')
echo "contract_name in smart_oracle.yml is ${contract_name}"

admin_token=$(grep "admin_token:" ./config_files/smart_oracle.yml | awk '{print $2}')
echo "admin_token in smart_oracle.yml is ${admin_token}"

echo "input oracle-server http address : (as localhost:10123)"
read address #监听地址

echo "input chain alias :(as chain1_alias) "
read chain_alias #合约安装的链id

echo "input contract-version , be integer in range [1,+inf]: "
read contract_version 

echo "input 1 means install_contract (default) , 2 means upgrade_contract"
read operate

apiAction=install_contract
if [ "$operate" -eq 2 ];then
    apiAction=upgrade_contract
fi 
echo "your choice is ${apiAction}"

cp ./standard_oracle_contract/oracle_contract_file/oracle_contract_v1 ./"$contract_name"
contract_7z="$contract_name".7z
7z a "$contract_7z" "$contract_name"

path=`pwd`/

oFile=${path}${contract_name}
oFile7=${path}${contract_7z}
echo $oFile
echo $oFile7

echo "curl -H 'admin_auth_token: ${admin_token}' -X POST ${address}/v1/${apiAction} -F 'chain_alias=${chain_alias}' -F 'contract_version=${contract_version}' -F 'runtime=DOCKER_GO' -F 'contract_file=@${oFile7}'"

curl -H "admin_auth_token: ${admin_token}" -X POST ${address}/v1/${apiAction} -F "chain_alias=${chain_alias}" -F "contract_version=${contract_version}" -F "runtime=DOCKER_GO" -F "contract_file=@${oFile7}"

rm $oFile
rm $oFile7