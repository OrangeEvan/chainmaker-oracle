/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package models

// EventQueryType 查询类型枚举
type EventQueryType int

const (
	QueryTypeHttp       EventQueryType = iota + 1 // QueryTypeHttp http查询类型
	QueryTypeMysql                                // QueryTypeMysql mysql查询类型
	QueryTypeGetVRF                               // QueryTypeGetVRF 获取随机数
	QueryTypeVerifyVRF                            // QueryTypeVerifyVRF 验证随机数
	QueryTypeCrossChain                           // QueryTypeCrossChain 跨链查询
)

var (
	QueryHttpMethod       = "queryHttp"       // QueryHttpMethod 查询http
	QueryMysqlMethod      = "queryMysql"      // QueryMysqlMethod 查询mysql
	QueryVRFMethod        = "getVrf"          // QueryVRFMethod 查询vrf
	QueryCrossChainMethod = "queryCrossChain" // QueryCrossChainMethod 跨链查询
	VerifyMethod          = "verifyVrf"       // VerifyMethod 验证VRF
)
