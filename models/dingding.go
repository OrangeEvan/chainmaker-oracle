/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package models

// DingdingMessage 钉钉消息模型
type DingdingMessage struct {
	MsgType string      `json:"msgtype"`
	Text    TextContent `json:"text"`
}

// DingdingResp 钉钉http接口返回数据
type DingdingResp struct {
	ErrCode   int    `json:"errcode"`
	ErrMsg    string `json:"errmsg"`
	MessageId string `json:"messageId"`
}
