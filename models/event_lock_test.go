/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package models

import (
	"chainmaker-oracle/databases"
	"fmt"
	"testing"
	"time"
)

func TestInsertEventLock(t *testing.T) {
	databases.GSmartOracleDB = initSqlite3()
	defer databases.GSmartOracleDB.Close()
	db := &EventLock{
		RequestId:     "weji9238902dcc",
		LockedVersion: 1,
		UpdatedAt:     time.Now(),
	}
	insertErr := InsertEventLock(db)
	if insertErr != nil {
		fmt.Printf("insert err , %s ", insertErr.Error())
		return
	}

}

func TestUpdateEventLock(t *testing.T) {
	databases.GSmartOracleDB = initSqlite3()
	defer databases.GSmartOracleDB.Close()
	db := &EventLock{
		RequestId:     "weji9238902dcc",
		LockedVersion: 1,
		UpdatedAt:     time.Now(),
	}
	insertErr := InsertEventLock(db)
	if insertErr != nil {
		fmt.Printf("insert err , %s ", insertErr.Error())
		return
	}

	uErr := UpdateEventLock(db)
	if uErr != nil {
		fmt.Printf("update err, %s ", uErr.Error())
		return
	}

}

func TestUpdateLock(t *testing.T) {
	databases.GSmartOracleDB = initSqlite3()
	defer databases.GSmartOracleDB.Close()
	db := &EventLock{
		RequestId:     "weji9238902dcc",
		LockedVersion: 1,
		UpdatedAt:     time.Now(),
	}
	insertErr := InsertEventLock(db)
	if insertErr != nil {
		fmt.Printf("insert err , %s ", insertErr.Error())
		return
	}

	uErr := UpdateLock(db)
	if uErr != nil {
		fmt.Printf("update err, %s ", uErr.Error())
		return
	}
}

func TestQueryEventLock(t *testing.T) {
	databases.GSmartOracleDB = initSqlite3()
	defer databases.GSmartOracleDB.Close()
	db := &EventLock{
		RequestId:     "weji9238902dcc",
		LockedVersion: 1,
		UpdatedAt:     time.Now(),
	}
	insertErr := InsertEventLock(db)
	if insertErr != nil {
		fmt.Printf("insert err , %s ", insertErr.Error())
		return
	}
	data, dataErr := QueryEventLock(db.RequestId)
	if dataErr != nil {
		fmt.Printf("queryErr err %s ", dataErr.Error())
		return
	}
	fmt.Printf("%+v  ", data)
}
