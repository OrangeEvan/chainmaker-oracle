/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package models

import (
	"chainmaker-oracle/databases"
	"fmt"
	"testing"
)

func TestQueryVRFStore(t *testing.T) {
	databases.GSmartOracleDB = initSqlite3()
	defer databases.CloseDB()
	data := &VRFStore{
		PrivateKeys: []byte{'1'},
		PublicKeys:  []byte{'2'},
		SrvName:     srvNameUniq,
	}
	err := SaveVRFStore(data)
	if err != nil {
		fmt.Println("save error : ", err.Error())
	}
	data2, data2Err := QueryVRFStore()
	if data2Err != nil {
		fmt.Printf("query error , %s ", data2Err.Error())
		return
	}
	fmt.Printf("query got %+v ", data2)
}

func TestSaveVRFStore(t *testing.T) {
	databases.GSmartOracleDB = initSqlite3()
	defer databases.CloseDB()
	data := &VRFStore{
		PrivateKeys: []byte{'1'},
		PublicKeys:  []byte{'2'},
		SrvName:     srvNameUniq,
	}
	err := SaveVRFStore(data)
	if err != nil {
		fmt.Println("save error : ", err.Error())
	}

}
