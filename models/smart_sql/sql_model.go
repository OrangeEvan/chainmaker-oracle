/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

// Package smart_sql 定义了合约查询sql数据库的模型
package smart_sql

// ModelSql 查询sql数据库的模型
type ModelSql struct {
	SqlType     string `json:"sql_type"` //select
	SqlSentence string `json:"sql_sentence"`
	DataSource  string `json:"data_source"` //数据源
}
