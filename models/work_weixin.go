/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package models

// WeixinTextMessage 企业微信文本消息模型
type WeixinTextMessage struct {
	MsgType string      `json:"msgtype"`
	Text    TextContent `json:"text"`
}

// TextContent 消息体
type TextContent struct {
	Content string `json:"content"`
}

var (
	GlobalWorkWeixinTypeText = "text" // GlobalWorkWeixinTypeText 企业微信“文本”消息类型
)
