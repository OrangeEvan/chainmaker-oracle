/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package alarms

import (
	"chainmaker-oracle/config"
	"fmt"
	"os"
	"testing"
)

func TestSendAlarm(t *testing.T) {
	initTestTools()
	wp := &WeixinAlarmSender{}
	sendErr := wp.SendAlarm("call chain error xxx")
	if sendErr != nil {
		fmt.Printf("send error : %s  ", sendErr.Error())
	}
}

func initTestTools() {
	config.ReadConfigFile("../config_files/smart_oracle.yml")
	os.Chdir("../")
}
