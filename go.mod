module chainmaker-oracle

go 1.16

require (
	chainmaker.org/chainmaker/common/v2 v2.2.1
	chainmaker.org/chainmaker/pb-go/v2 v2.2.1
	chainmaker.org/chainmaker/sdk-go/v2 v2.2.1
	github.com/agiledragon/gomonkey v2.0.2+incompatible
	github.com/antchfx/htmlquery v1.2.4
	github.com/antchfx/jsonquery v1.1.5
	github.com/antchfx/xmlquery v1.3.10
	github.com/gin-gonic/gin v1.7.7
	github.com/go-sql-driver/mysql v1.6.0
	github.com/jmoiron/sqlx v1.3.5
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/panjf2000/ants/v2 v2.5.0
	github.com/robfig/cron/v3 v3.0.1
	github.com/smartystreets/goconvey v1.7.2
	github.com/spf13/viper v1.11.0
	go.uber.org/zap v1.21.0
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
